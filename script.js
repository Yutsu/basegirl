let form = document.getElementById('form');
let small = document.getElementById('small');

form.addEventListener('submit', e => {
    e.preventDefault();
    let emailValue = form['email'].value;

    if(!emailValue) {
        form.classList.add('error');
        small.innerHTML ="Email empty";
    }
    else if(!validateEmail(emailValue)){
        form.classList.add('error');
        small.innerHTML ="Email invalid";
    }
    else {
        form.classList.remove('error');
        document.body.innerHTML =
            `<div style="display: flex; align-items: center; justify-content: center; font-size: 50px;">
                <h1>Thanks !</h1>
            </div> `;
    }
});


function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}